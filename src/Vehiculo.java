/**
 * @author BELSOFT
 */
public class Vehiculo {
    String marca;
    String paisOrigen;

    /**
     * Accion que permite que el vehiculo arranque a una velocidad determinada.
     */
    public void prender(){
        System.out.println("El Vehiculo con marca "+ marca +" ha encendido");
    }
}

class Pieza{
    
}