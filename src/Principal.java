/**
 *
 * @author BELSOFT
 */
public class Principal {

    public static void main(String[] args) {
        //En la siguiente línea se utiliza el metodo para mostrar mensaje en consola
        System.out.println("¡Hola mundo!");

        /*
        Oh gloria, inmarcesible, oh jubilo inmortal.
        La siguiente sección de código sirve para validar los campos
        y comportamientos que pueden tener los vehiculos
        */
        Vehiculo veh1 = new Vehiculo();
        veh1.marca="BMW";
        veh1.prender();
        
        Vehiculo veh2 = new Vehiculo();
        veh2.marca="HYNDAI";
        veh2.prender();
       
        Pieza pieza=new Pieza();
    }
    
}
